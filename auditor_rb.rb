require 'optparse'
require 'pp'
require 'rugged'
require 'logger'

$options = {}
OptionParser.new do |opts|
  opts.banner = "Usage: auditor.rb [$options]"
  opts.on("--repo repo", "Local Git repo directory") { |repo| $options[:repo] = repo }
  opts.on("--backward backward", "backward branch") { |backward| $options[:backward] = backward }
  opts.on("--forward forward", "forward branch") { |forward| $options[:forward] = forward }
  opts.on("--ignore ignore", "commits to ignore") { |ignore| $options[:ignore] = ignore}
end.parse!

if $options[:repo].nil? || $options[:backward].nil? || $options[:forward].nil?
  raise "Invalid arguments."
end

$logger = Logger.new("/home/ec2-user/jenkins/workspace/Auditor/debug-#{$options[:repo].split("/")[6]}-forward-#{$options[:forward]}-backward-#{$options[:backward]}.log")

def get_list_of_commits(repository, branch)
  repository.checkout(branch)
  walker = Rugged::Walker.new(repository)
  last_common_commit = repository.merge_base($options[:forward], $options[:backward])
  walker.push(repository.head.target_id)
  commits_list = []
  walker.each do |commit|
    if commit.oid.eql? last_common_commit
      break
    elsif commit.author[:email] == "jenkins@qubole.com"
      next
    elsif commit.author[:email] == "dev@qubole.com"
      next
    elsif commit.author[:email].include?  "@nandi.prod.qubole.net"
      next
    elsif commit.parents.size > 1
      next
    end
    commits_list.push(commit)
  end
  return commits_list
  
end

def get_commits_to_ignore(ignore)

  commits_to_ignore = []
  File.readlines(ignore).each do |line|
    commits_to_ignore.push(line.chomp)
  end
  return commits_to_ignore

end

def audit(repo, backward, forward)
  repository = Rugged::Repository.new(repo)

  branch_collection = Rugged::BranchCollection.new(repository)

  last_common_commit = repository.merge_base($options[:forward], $options[:backward])

  commits_in_backward = get_list_of_commits(repository, backward)
  commits_in_forward = get_list_of_commits(repository, forward)

  
  debug = `cd #{repo} && git checkout #{forward} 2>&1`
  # $logger.debug(debug)
  target_commit = repository.head.target_id

  commits_to_ignore = get_commits_to_ignore($options[:ignore])

  commits_in_backward.reverse.each do |commit_backward|
    #$logger.info("Checking for commit: #{commit_backward.oid} in #{backward}")

    
    if commits_to_ignore.include? commit_backward.oid
      #$logger.info("000000000000 NIL for commit: #{commit_backward.oid} in BRANCH: #{backward} by user: #{commit_backward.author[:email]}\n")
      next
    end
    
    
   
    flag = 1
    

    commits_in_forward.reverse.each do |commit_forward|
      flag = 0

      debug = `cd  #{repo} && git checkout -b test-forward-branch  2>&1`
      # $logger.debug(debug)

      debug = `cd  #{repo} && git reset --hard #{commit_forward.oid}  2>&1`
      # $logger.debug(debug)

      debug = `cd  #{repo} && git cherry-pick --keep-redundant-commits #{commit_backward.oid}  2>&1`
      #$logger.debug("#{debug} \n\n")

      diff = `cd  #{repo} && git diff HEAD^ HEAD`
      
      debug = `cd  #{repo} && git checkout 2>&1 #{forward} && git branch -D test-forward-branch  2>&1`
      # $logger.debug(debug)
      
     # puts commit_forward.oid
      if diff == ""
        #$logger.info("000000000000 NIL for commit: #{commit_backward.oid} in BRANCH: #{backward} by user: #{commit_backward.author[:email]}\nResolved by commit: #{commit_forward.oid}, author: #{commit_forward.author[:email]}, message: #{commit_forward.message} in BRANCH: #{forward}\n\n\n")
        flag = 1
        break
      else
        #$logger.debug("Diff not empty after comapring #{commit_backward.oid} in BRANCH: #{backward} and #{commit_forward.oid} in BRANCH: #{forward}\n\n\n")
      end
    end
    if flag == 0
      $logger.info("Not NIL for commit: #{commit_backward.oid} by Author: #{commit_backward.author[:email]}. Message: #{commit_backward.message}\n\n\n")
    end
    # `cd #{repo} && git checkout #{forward} && git branch -D test-forward-branch`
    
  end
###########################################################
end

audit($options[:repo],$options[:backward],$options[:forward])

=begin
1. find common ancestor point
2. for all commits after ancestor point in backward branch
  a. create test-forward-branch
  b. git cherry-pick --allow-empty with commit id
  c. git commit --allow-empty
  d. see if previous commit has a diff.
    i. if not, all good
    ii. if yes, trigger an email to author & gayathrym


bonus: figure out where to keep a list of ignored commits
=end